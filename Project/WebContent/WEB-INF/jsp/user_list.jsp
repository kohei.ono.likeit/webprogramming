<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>ユーザ一覧</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>

	<div class="row justify-content-end">
		<div class="col-sm-2">
			<p>${userInfo.name}さん</p>
		</div>
		<div class="col-sm-2">
			<a href="LogoutServlet">ログアウト</a>
		</div>
	</div>

	<div class="row justify-content-center">
		<h1>ユーザ一覧</h1>
	</div>

	<div class="row justify-content-end">
		<div class="col-sm-2">
			<a href="UserFormServlet">新規登録</a>
		</div>
	</div>

	<!--    ログインID入力-->
	<form class="form-horizontal" action="UserListServlet" method="post">
		;
		<div class="row justify-content-center">
			<div class="col-sm-2">
				<P>ログインID</P>
			</div>
			<div class="col-sm-2">
				<input type="text" name="loginId" style="width: 300px;">
			</div>
		</div>

		<!--    ユーザ名入力-->
		<div class="row justify-content-center">
			<div class="col-sm-2">
				<P>ユーザ名</P>
			</div>
			<div class="col-sm-2">
				<input type="text" name="name" style="width: 300px;">
			</div>
		</div>

		<!--    生年月日入力-->
		<div class="row justify-content-center">
			<div class="col-sm-2">
				<P>生年月日</P>
			</div>
			<div class="col-sm-1">
				<input type="date" name="date-start" style="width: 150px;">
			</div>


			<div class="col-sm-1">
				<input type="date" name="date-end" style="width: 150px;">
			</div>
		</div>

		<div class="row justify-content-end">
			<div class="col-sm-3">
				<input type="submit" value="検索" style="width: 200px">
			</div>
		</div>
	</form>


	<div class="row justify-content-center">
		<table border="1">
			<tr>
				<th>ログインID</th>
				<th>ユーザ名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
			<c:forEach var="user" items="${userList}">
				<tr>
					<td>${user.loginId}</td>
					<td>${user.name}</td>
					<td>${user.birthDate}</td>
					<td>
						<a class="btn btn-primary"
							href="UserDetailServlet?id=${user.id}">詳細</a>
							<c:if test="${userInfo.name == '管理者' or userInfo.name == user.name}">
								<a class="btn btn-success"
									href="UserUpdateServlet?id=${user.id}">更新</a>
							</c:if>

							<c:if test="${userInfo.name == '管理者'}">
								<a class="btn btn-danger"
									 href="UserDeleteServlet?id=${user.id}">削除</a>
							</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>

</html>
