<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ削除確認</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

    <div class="row justify-content-end">
        <div class="col-sm-2">
            <p>${userInfo.name}さん</p>
        </div>
        <div class="col-sm-2">
            <a href="LogoutServlet">ログアウト</a>
        </div>
    </div>

    <div class="row justify-content-center">
        <h1>ユーザ削除確認</h1>
    </div>


	<form class="form-delete"action="UserDeleteServlet" method="post">
	<input type="hidden" name="id" value="${user.id}">
	<!--    削除確認文-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>ログインID</P>
	        </div>
	        <div class="col-sm-2">
	        <input type="hidden" name="loginId" value="${user.loginId}">
	            <P>${user.loginId}</P>
	        </div>
	    </div>

	    <div class="row justify-content-center">
	    	<div class="col-sm-4">
	 	   <p>を本当に削除してよろしいでしょうか。</p>
	 	   </div>
		</div>

	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <input type="submit" name="CancelButton" value="キャンセル" style="width: 200px">
	        </div>

	        <div class="col-sm-2">
	            <input type="submit" name="OkButton" value="OK" style="width: 200px">
	        </div>
	    </div>
	</form>
</body>
</html>