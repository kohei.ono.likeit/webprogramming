<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

    <div class="row justify-content-end">
        <div class="col-sm-2">
            <p>${userInfo.name}さん</p>
        </div>
        <div class="col-sm-2">
            <a href="LogoutServlet">ログアウト</a>
        </div>
    </div>

    <div class="row justify-content-center">
        <h1>ユーザ情報詳細参照</h1>
    </div>


<!--    ログインID-->
    <div class="row justify-content-center">
        <div class="col-sm-2">
            <P>ログインID</P>
        </div>
        <div class="col-sm-2">
            <P>${user.loginId}</P>
        </div>
    </div>

<!--    ユーザ名-->
    <div class="row justify-content-center">
        <div class="col-sm-2">
            <P>ユーザ名</P>
        </div>
        <div class="col-sm-2">
            <P>${user.name}</P>
        </div>
    </div>

<!--    生年月日-->
    <div class="row justify-content-center">
        <div class="col-sm-2">
            <P>生年月日</P>
        </div>
        <div class="col-sm-2">
            <P>${user.birthDate}</P>
        </div>
    </div>

    <!--    登録日時-->
    <div class="row justify-content-center">
        <div class="col-sm-2">
            <P>登録日時</P>
        </div>
        <div class="col-sm-2">
            <P>${user.createDate}</P>
        </div>
    </div>

<!--    更新日時-->
    <div class="row justify-content-center">
        <div class="col-sm-2">
            <P>更新日時</P>
        </div>
        <div class="col-sm-2">
            <P>${user.updateDate}</P>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-sm-4">
            <a href="UserListServlet">戻る</a>
        </div>
    </div>

</body>
</html>