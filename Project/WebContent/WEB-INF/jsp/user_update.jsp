<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

    <div class="row justify-content-end">
        <div class="col-sm-2">
            <p>${userInfo.name}さん</p>
        </div>
        <div class="col-sm-2">
            <a href="LogoutServlet">ログアウト</a>
        </div>
    </div>

    <div class="row justify-content-center">
        <h1>ユーザ情報更新</h1>
    </div>

    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger row justify-content-center" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<form class="form-signin"action="UserUpdateServlet" method="post">
	<input type="hidden" name="id" value="${user.id}">
	<!--    ログインID-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>ログインID</P>
	        </div>
	        <div class="col-sm-2">
	            <P>${user.loginId}</P>
	        </div>
	    </div>

	<!--    パスワード更新-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>パスワード</P>
	        </div>

	        <div class="col-sm-2">
	            <input type="password" name="password" style="width:200px;">
	        </div>
	    </div>

	<!--    パスワード(確認)-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>パスワード(確認)</P>
	        </div>

	        <div class="col-sm-2">
	            <input type="password" name="passwordCheck" style="width:200px;">
	        </div>
	    </div>

	    <!--    ユーザ名-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>ユーザ名</P>
	        </div>

	        <div class="col-sm-2">
	            <input type="text" name="name" style="width:200px;" value="${user.name}">
	        </div>
	    </div>

	<!--    生年月日-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>生年月日</P>
	        </div>

	        <div class="col-sm-2">
	            <input type="date" name="birthDate" style="width:200px;" value="${user.birthDate}">
	        </div>
	    </div>


	    <div class="row justify-content-center">
	        <input type="submit" value="更新" style="width: 200px">
	    </div>
	</form>

    <div class="row justify-content-center">
        <div class="col-sm-4">
            <a href="UserListServlet">戻る</a>
        </div>
    </div>
