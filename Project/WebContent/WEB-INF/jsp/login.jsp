<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

    <div class="row justify-content-center">
        <h1>ログイン画面</h1>
    </div>

    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger row justify-content-center" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<form class="form-signin"action="LoginServlet" method="post">
<!--    ログインID入力-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>ログインID</P>
	        </div>
	        <div class="col-sm-2">
	            <input type="text" name="loginId" style="width:200px;">
	        </div>
	    </div>
<!--    パスワード入力-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>パスワード</P>
	        </div>
	        <div class="col-sm-2">
	            <input type="password" name="password" style="width:200px;">
	        </div>
	    </div>
	    <div class="row justify-content-center">
	        <input type="submit" value="ログイン" style="width: 200px">
	    </div>
	</form>
</body>
</html>