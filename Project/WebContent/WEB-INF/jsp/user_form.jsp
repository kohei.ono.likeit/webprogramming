<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ新規登録</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>

    <div class="row justify-content-end">
        <div class="col-sm-2">
            <p>${userInfo.name}さん</p>
        </div>
        <div class="col-sm-2">
            <a href="LogoutServlet">ログアウト</a>
        </div>
    </div>

    <div class="row justify-content-center">
        <h1>ユーザ新規登録</h1>
    </div>

    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger row justify-content-center" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<form class="form-signin"action="UserFormServlet" method="post">
<!--    ログインID入力-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>ログインID</P>
	        </div>
	        <div class="col-sm-2">
	            <input type="text" name="loginId" style="width:200px;">
	        </div>
	    </div>

<!--    パスワード入力-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>パスワード</P>
	        </div>
	        <div class="col-sm-2">
	            <input type="password" name="password" style="width:200px;">
	        </div>
	    </div>

<!--    パスワード確認-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>パスワード(確認)</P>
	        </div>
	        <div class="col-sm-2">
	            <input type="password" name="passwordCheck" style="width:200px;">
	        </div>
	    </div>

<!--    ユーザ名入力-->
	    <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>ユーザ名</P>
	        </div>
	        <div class="col-sm-2">
	            <input type="text" name="name" style="width:200px;">
	        </div>
	    </div>

<!--    生年月日入力-->
	     <div class="row justify-content-center">
	        <div class="col-sm-2">
	            <P>生年月日</P>
	        </div>
	        <div class="col-sm-2">
	            <input type="text" name="birthDate" style="width:200px;">
	        </div>
	    </div>

	    <div class="row justify-content-center">
	            <input type="submit" value="登録" style="width: 200px">
	    </div>
	</form>

    <div class="row justify-content-center">
        <div class="col-sm-4">
            <a href="UserListServlet">戻る</a>
        </div>
    </div>

</body>
</html>