package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		セッションタイムアウト
		HttpSession session = request.getSession(false);
		User u=(User)session.getAttribute("userInfo");

		if (u == null){
			response.sendRedirect("LoginServlet");
			return;
		}

		String id =request.getParameter("id");
		System.out.println(id);

		UserDao userDao = new UserDao();
		User user = userDao.UserUpdate(id);

		HttpSession h = request.getSession();
		h.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordCheck");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String id = request.getParameter("id");


//		分岐1 ユーザー名 生年月日が未入力
		if (name.isEmpty() || birthDate.isEmpty()) {
			request.setAttribute("errMsg","入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;
		}

//		分岐2 パスワードが異なる場合
		if (!(password.equals(passwordCheck)) ) {
			request.setAttribute("errMsg","入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_update.jsp");
			dispatcher.forward(request, response);
			return;
		}

//		分岐3 パスワード未入力の場合
		if (password.isEmpty() && passwordCheck.isEmpty()) {
			UserDao userDao=new UserDao();
			User user=userDao.UserUpdateNext(password,passwordCheck,name,birthDate,id);
		}

		UserDao userDao=new UserDao();
		String userPass=userDao.UserPass(password);
		User user=userDao.UserUpdateNext(userPass,passwordCheck,name,birthDate,id);

		response.sendRedirect("UserListServlet");
	}
}
