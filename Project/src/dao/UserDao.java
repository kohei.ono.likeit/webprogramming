package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

//ログイン
public class UserDao {
	//	LoginServletへ戻す
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String sql = "select * from user where login_id=? and password=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			return new User(loginIdDate, nameDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

//ユーザーリスト表示
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user where login_id !='admin'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	//ユーザー検索
		public List<User> findSearch(String loginIdP,String nameP,String startDate,String endDate) {
			Connection conn = null;
			List<User> userList = new ArrayList<User>();

			try {

				conn = DBManager.getConnection();

				String sql = "SELECT * FROM user where login_id !='admin'";

				if(!loginIdP.equals("")) {
					sql += " AND login_id = '" + loginIdP + "'";
				}

				if(!nameP.equals("")) {
					sql += "AND name LIKE '%" + nameP + "%'";
				}

				if(!startDate.equals("")) {
					sql += "AND birth_date >= '" + startDate +"'";
				}

				if(!endDate.equals("")) {
					sql += "AND birth_date <= '"+ endDate +"'";
				}

				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);

				while (rs.next()) {
					int id = rs.getInt("id");
					String loginId = rs.getString("login_id");
					String name = rs.getString("name");
					Date birthDate = rs.getDate("birth_date");
					String password = rs.getString("password");
					String createDate = rs.getString("create_date");
					String updateDate = rs.getString("update_date");
					User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

					userList.add(user);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {

				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return userList;
		}

//ユーザー登録
	public User UserFormDao(String loginId, String password, String passwordCheck, String name, String birthDate) {
		//		UserFormServletに戻す
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "insert into user (login_id,password,name,birth_date,create_date,update_date)values(?,?,?,?,NOW(),NOW())";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
	}

//	ユーザー登録　エラー表示
	public User UserFormNext(String loginId) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "select * from user where login_id=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginIdDate = rs.getString("login_id");
			return new User(loginIdDate);

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
	}

	//ユーザー登録 暗号化
		public String UserPass(String password) {
			Charset charset = StandardCharsets.UTF_8;

			String algorithm = "MD5";

			String result = "";
			try {
				byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
				result = DatatypeConverter.printHexBinary(bytes);
				System.out.println(result);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			return result;
		}

//	ユーザー詳細
	public User UserDetail(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "select * from user where id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int UserId = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			return new User(UserId,loginId,name,birthDate,createDate,updateDate);


		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

//	ユーザー情報更新
	public User UserUpdate (String id){
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "select * from user where id=?";
//			String sqlUpdate ="update user where password=? and name=? and birth_date=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);

			ResultSet rs = pStmt.executeQuery();

//			PreparedStatement pStmtUpdate = conn.prepareStatement(sqlUpdate);
//			int num = pStmtUpdate.executeUpdate();

			if (!rs.next()) {
				return null;
			}

			int UserId = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String password = rs.getString("password");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");

			return new User(UserId,loginId,password,name,birthDate);


		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public User UserUpdateNext(String password, String passwordCheck,String name, String birthDate,String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sqlUpdate ="update user set password=?,name=?,birth_date=?,update_date=now() where id=?";

			PreparedStatement pStmtUpdate = conn.prepareStatement(sqlUpdate);

			pStmtUpdate.setString(1, password);
			pStmtUpdate.setString(2, name);
			pStmtUpdate.setString(3, birthDate);
			pStmtUpdate.setString(4,id);

			pStmtUpdate.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

//	ユーザー削除
	public User UserDelete(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "select * from user where id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int UserId = rs.getInt("id");
			String loginId = rs.getString("login_id");

			return new User(UserId,loginId);

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public User UserDeleteNext(String loginId) {
	Connection conn = null;
	try {
		conn = DBManager.getConnection();
		String sqlDelete ="delete from user where login_id=?";

		PreparedStatement pStmtDelete = conn.prepareStatement(sqlDelete);

		pStmtDelete.setString(1,loginId);
		pStmtDelete.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		return null;
	}
}
