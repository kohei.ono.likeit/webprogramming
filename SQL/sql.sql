﻿create database usermanagement default character set utf8;

use usermanagement;

create table user(id serial,
login_id varchar(255) unique not null,
name varchar(255) not null,
birth_date date not null,
password varchar(255) not null,
create_date datetime not null,
update_date datetime not null
);

insert into user
values(update_date,cast(now()as datetime));

insert into user(login_id,name,birth_date,password,create_date,update_date) 
values ('admin','管理者','2020-01-01','password',NOW(),NOW());

insert into user(login_id,name,birth_date,password,create_date,update_date) 
values ('aaa','ユーザー1','2020-01-02','password',NOW(),NOW());


insert into user(login_id,name,birth_date,password,create_date,update_date) 
values ('bbb','ユーザー2','2020-02-21','hoge',NOW(),NOW());

select * from user;

select * from user where login_id='ccc' and password='password' and name='ユーザー3' and birth_date='2020-03-03';

